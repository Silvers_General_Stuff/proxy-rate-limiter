import http from 'http'
import Queue from 'smart-request-balancer';
import axios, {AxiosRequestConfig, Method} from 'axios';
import Logger, {LoggingConfig} from "./Logger";
import fs from "fs";

interface DataJSON {
    rules: {
        [propName: string]: {
            rate: number
            limit: number
            priority: number
        };
    }
    logging: LoggingConfig
    interfaces: string;
    port: number
}
function get_config (config_path: string): DataJSON {
    try {
        return JSON.parse(fs.readFileSync(config_path, 'utf8'));
    } catch (err) {
        console.log(err)
        // set reasonable defaults
        return {
            rules: {
                guildwars2: {
                    rate: 200,
                    limit: 60,
                    priority: 1
                },
                common: {
                    rate: 250,
                    limit: 60,
                    priority: 1
                }
            },
            logging: {
                outputs: ["console"],
                file: "",
                grey_log: "",
                date: ""
            },
            interfaces: "*",
            port: 3333
        }
    }
}

const onRequest = (client_req, client_res, ctx: CTX) => {
    let start = new Date().getTime()
    if(
        // if set to " allow all interfaces
        ctx.config.interfaces !== "*" &&
        // host is eiterh localhost or IP or a hostname
        !client_req.headers["host"].startsWith(ctx.config.interfaces)
    ){
        // ignore all such requests
        client_res.end()
        return
    }

    let toGet = client_req.headers["proxy-conf"]
    if(typeof toGet !== "string"){
        client_res.write('No proxy-conf header stringified object supplied');
        client_res.end()
        return
    }

    try{
        toGet = JSON.parse(toGet)
    }catch{
        client_res.write('proxy-conf nor parsable into object');
        client_res.end()
        return
    }

    let [config, category, program] = generateConfig(toGet, client_req, ctx)

    if(config.url === "" ){
        client_res.write('No url supplied in proxy-conf header');
        client_res.end()
        return
    }

    proxy(config, category,client_res, start, program, ctx)
}

class toGet extends Object {
    url: string;
    category?: string;
    method?: Method;
    headers?: any;
    config?: AxiosRequestConfig;
    program?: string;
}

const generateConfig = (toGet: toGet, client_req, ctx: CTX):[AxiosRequestConfig, string, string] =>{
    // for the options object first
    let config:AxiosRequestConfig

    // allow a config objec tot be passed through
    if(typeof toGet.config !== "undefined"){
        config = toGet.config
    }else{
        let headers = toGet.headers || {}
        if(typeof toGet.headers === "undefined") {
            // Copy headers from request to proxy if headers are not in config
            delete client_req.headers["proxy-conf"]
            delete client_req.headers["host"]
            delete client_req.headers["connection"]
            headers = Object.assign({}, client_req.headers)
        }

        // basic configh
        config = {
            url: toGet.url || "",
            method: toGet.method || "get",
            responseType: 'stream',
            headers: headers,
        }
    }

    // sort out teh category here
    let category = toGet.category || "common"
    if(typeof ctx.config.rules[category] === "undefined"){
        category = "common"
    }

    // sort out teh category here
    let program = toGet.program || "unnamed"

    return [config, category, program]
}

const proxy = (config, category, client_res, start, program, ctx: CTX) => {
    ctx.queue.request((retry) => sendRequest(retry,config,category, ctx), config.url, category)
        // our actual response
        .then(res => {
            ctx.logger.log("info",createLogData(config, category, start, res, program))
            if(typeof res === "undefined" || typeof res.status === "undefined"){
                client_res.end()
                return
            }
            res.headers["transfer-encoding"] = "chunked"
            if(res.headers["content-length"]){
                delete res.headers["content-length"]
            }
            client_res.writeHead(res.status, res.headers)
            res.data.pipe(client_res, {end: true})
        })
        .catch(error => ctx.logger.log("error",error));
}

const sendRequest = (retry,config,category, ctx: CTX) =>{
    return axios(config)
        .catch(error => {
            try {
                // We've got 429 - too many requests
                if (error.response.status === 429) {
                    // usually 300 seconds
                    return retry((ctx.config.rules[category].limit / ctx.config.rules[category].rate) + 5)
                }
            }catch (e) {
                //Do nothing and return
            }
            return error.response
        })
}

const createLogData = (config, category, startTime,res, program) =>{
    return {
        url: config.url.split(/[?#]/)[0],
        method: config.method,
        category: category,
        duration: new Date().getTime() - startTime,
        timestamp: startTime,
        status: res.status || 400,
        program: program
    }
}

interface CTX {
    config: DataJSON
    logger: Logger
    queue: Queue
}

function main (){
    // get arguments ehre

    let config_path = "./data.json"
    if(process.argv.length > 2){
        config_path = process.argv[2];
    }

    let config = get_config(config_path);

    const logger = new Logger(config.logging)
    const queue = new Queue({rules: config.rules});

    let ctx: CTX = {
        config,
        logger,
        queue
    }

    let server = http.createServer((req, res) => onRequest(req, res, ctx));

    server.listen(config.port, () => {
        console.log(`Server started at ${config.port}`);
    });
}

main();