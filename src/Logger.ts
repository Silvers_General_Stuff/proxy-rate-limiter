import path from "path";
import fs from "fs"
import {name} from "../package.json"
import axios, {AxiosRequestConfig} from 'axios'

export class CustomError extends Error{
    misc?: string
}

interface GELF {
    version:string;
    host:string;
    short_message:string;
    full_message?:string;
    timestamp:number;
    level?:number;
    [propName: string]: any;
}

export interface LoggingConfig {
    outputs: Array<string>
    file: string
    grey_log: string
    date: string
}

export default class Logger {
    // set up attributes
    private readonly pathname: string;
    private message: Array<string>
    private location: Error;
    private outputs: Array<string>
    private file:string;
    private greylog: string;
    private date:string;

    constructor(logging: LoggingConfig) {
        // set up basic info

        this.pathname = path.resolve(".")
            // to deal with windows paths fecking it all up
            .replace(/\\/g, "\\\\");


        this.message = []
        this.outputs = logging.outputs || ["console"]
        this.file = logging.file || "./logs"
        this.greylog = logging.grey_log || ""
        this.date = logging.date || "ISO"
    }

    log(type:string, message:any, location: Error = new Error()){
        this.location = location

        // if the error type is used then the error is processed to unclude the position of the error

        let date = this.getDate()

        // output to console first
        if(this.outputs.indexOf("console") !== -1){
            this.toConsole(type, date, message)
        }

        // then filesystem
        if(this.outputs.indexOf("file") !== -1){
            this.toFile(type, date, message)
        }

        // finally to greylog
        if(this.outputs.indexOf("greylog") !== -1){
            this.toGreylog(type, message)
        }
    }

    getDate(override_format = this.date){
        let date
        switch(override_format){
            case "epoch_ms":{
                date = new Date().getTime()
                break
            }
            case "epoch_s":{
                date = new Date().getTime()/1000
                break
            }
            default: {
                date = new Date().toISOString()
            }
        }
        return date
    }

    toConsole(type, date, message){
        if(type === "error"){
            this.processError(message as Error)
            console.error(date, this.message.join('\n'))
        }else{
            if(typeof message === "object"){
                console.log(date, JSON.stringify(message))
            }else{
                console.log(date, message)
            }
        }
    }

    toFile(type, date, message){
        let stream = fs.createWriteStream(this.file, {flags:'a'})

        let processed
        if(type === "error"){
            this.processError(message as Error)
            processed = this.message
        }else{
            if(typeof message === "object"){
                processed = JSON.stringify(message)
            }else{
                if(typeof message === "string"){
                    processed = message
                }else{
                    this.toConsole("error",date,message)
                    stream.end()
                    return
                }
            }
        }
        stream.write(`${date} ${processed} \n`)
        stream.end()
    }

    toGreylog(type, message){
        if(this.greylog === ""){return}

        let messageToSend:GELF = {
            version:"1.1",
            host:name,
            short_message:type,
            full_message:message,
            // gelf needs date in seconds.miliseconds
            timestamp: this.getDate("epoch_s"),
        }

        if(typeof message === "object"){
            let keys = Object.keys(message)
            for(let i=0;i<keys.length;i++){
                let key = keys[i]
                if(key === "id"){continue}
                messageToSend[`_${key}`] = message[key]
            }
        }

        let config:AxiosRequestConfig = {
            method: 'POST',
            url: `${this.greylog}/gelf`,
            data: messageToSend
        }

        axios(config)
            .catch((err)=>
                this.toConsole("error", this.getDate(), err)
            )
    }


    private processError(error: Error) {
        // if there is a specific error message
        if ("misc" in error) {
            this.message.push((error as CustomError).misc)
        }
        // this is where we trace back to the source of the issue
        if (error.stack) {
            // replace all instances of the path
            error.stack = error.stack.replace(new RegExp(this.pathname, "gi"), '.');
            this.location.stack = this.location.stack.replace(new RegExp(this.pathname, "gi"), '.');

            // the actual error message does not contain teh actual location of teh issue, just what module caused it
            // so I am using the location var with is a new error() and stripping out what I want from taht and merging teh two together

            // split both into arrays
            let errorArray = error.stack.split("\n");
            let locationArray = this.location.stack.split("\n")

            let newError = [
                // first line from the error
                errorArray[0],
                // third line from teh location
                locationArray[2],
                // rest of the error
                ...errorArray.splice(1)
            ];

            this.message.push(newError.join("\n"))
        }
    }

}