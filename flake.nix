{
  # from https://github.com/jakeisnt/react-turn-based-game/blob/1b5f333fe308ad2b90fdf5a5aa6fd9761d774297/flake.nix
  description = "react turn based game";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }: utils.lib.eachDefaultSystem (system: 
    let
    
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ ];
      };

      package_name = "proxy-rate-limiter";
      
      # set the node version here
      nodejs = pkgs.pkgs.nodejs-16_x; 

    in rec {
    
      packages."${package_name}" = pkgs.mkYarnPackage {
        buildInputs = [
          nodejs
        ];
      
        src = self;
        packageJSON = "${./package.json}";
        yarnLock = "${./yarn.lock}";
        yarnPreBuild = "ls";
        buildPhase = "yarn build";
        #installPhase = "mkdir -p $out/build2; cp -R $out/libexec/proxy-rate-limiter/deps/proxy-rate-limiter/* $out/build2";
      };

      defaultPackage = packages."${package_name}";
      
      nixosModule = { lib, pkgs, config, ... }: 
        with lib; 
        let
          cfg = config.services."${package_name}";
        in { 
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";
            
            config = mkOption rec {
              type = types.str;
              default = "./data.json";
              example = default;
              description = "The config file";
            };
            
           # specific for teh program running
           prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
           };
           
           user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
           };
           
           home = mkOption rec {
              type = types.str;
              default = "/etc/${cfg.prefix}${package_name}";
              example = default;
              description = "The home for the user";
           };
            
          };

          config = mkIf cfg.enable {
          
            users.groups."${cfg.user}" = { };
            
            users.users."${cfg.user}" = {
              createHome = true;
              isSystemUser = true;
              home = "${cfg.home}";
              group = "${cfg.user}";
            };
            
            systemd.services."${cfg.prefix}${cfg.user}" = {
              description = "Proxy Rate Limiter";
              
              wantedBy = [ "multi-user.target" ];
              after = [ "network-online.target" ];
              wants = [ ];
              serviceConfig = {
                # fill figure this out in teh future
                #DynamicUser=true;
                User = "${cfg.user}";
                Group = "${cfg.user}";
                Restart = "always";
                ExecStart = "${nodejs}/bin/node ${self.defaultPackage."${system}"}/libexec/${package_name}/deps/${package_name} ${cfg.config}";
              };
            };
            
          };
          
        };

      
    });
}